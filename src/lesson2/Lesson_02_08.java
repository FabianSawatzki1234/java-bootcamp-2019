package lesson2;

public class Lesson_02_08 {

    public static void main(String[] args) {
    /* Bonus: Nested ifs
    Wir wollen entscheiden, ob eine Person Kaffee oder Tee-Junkie ist:

    Wenn die Anzahl von Kaffees und Tees beide größer 0 ist, wird "kein Favorit" ausgegeben.
    Wenn die Anzahl an Kaffees größer 0 und die Anzahl an Tees gleich 0 ist
        * und die Anzahl an Kaffees kleiner-gleich 3 ist, wird "favorisiert Kaffee" ausgegeben
        * und die Anzahl an Kaffees größer 3 ist, wird "Kaffee-Junkie" ausgegeben
    Wenn die Anzahl an Tees größer 0 und die Anzahl an Kaffees gleich 0 ist
        * und die Anzahl an Tees kleiner-gleich 3 ist, wird "favorisiert Tee" ausgegeben
        * und die Anzahl an Tees größer 3 ist, wird "Tee-Junkie" ausgegeben
    Wenn die Anzahl von beiden 0 ist, wird "unmöglich" ausgegeben.
     */

        int coffeeCount;
        int teaCount;
    }
}
