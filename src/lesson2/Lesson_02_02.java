package lesson2;

import static common.TrafficLightStatus.randomTrafficStatus;

public class Lesson_02_02 {

  public static void main(String[] args) {
    // Gib "Drive!" auf der Konsole aus, wenn die Ampel grün ist
    // Gib "Stop!" auf der Konsole aus, wenn die Ampel rot ist
    // Entscheide selbst, was du bei gelber Ampel schreibst
    String trafficLightStatus = randomTrafficStatus();
  }
}
