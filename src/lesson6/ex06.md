# Zoo

Erstelle ein Interface Animal mit den Methoden getName() und makeSound().

Erstelle beliebige Klassen (mindestens 3), die die Animal-Klasse implementieren.

Erstelle einen Zoo, der eine Anzahl von Tieren speichern kann und die Methoden add() und everyAnimalMakesASound() anbietet.
Letztere soll bei Aufruf alle Tiere in der Liste durchgehen und ihre jeweilige Methode makeSound() aufrufen.
Eine Besonderheit: statt eines Arrays nehmen wir hier eine ArrayList zum einfachen Abspeichern der Tiere.
* Initialisieren:  ArrayList<Animal> animals = new ArrayList<>();
* Hinzufügen:  animals.add(animal);

In der main-Methode soll ein Zoo mit mindestens drei unterschiedlichen Tieren angelegt werden.
Rufe anschließend die Methode everyAnimalMakesASound() auf.
