package lesson3;

import static common.Helper.getRandomIntBetween;

public class Lesson_03_02 {

  public static void main(String[] args) {
    // Definiere eine Function "weatherInterpreter". Diese nimmt einen Integer-Parameter "temperature" entgegen.
    // Schreibe je nach Temperatur verschiedene Nachrichten auf die Konsole.
    // Beispiel für Temperatur unter 0 Grad: "Es ist kalt draußen - nimm eine Jacke mit!"
    int temperature = getRandomIntBetween(-20, 50);
  }
}
