package lesson3;

public class Lesson_03_03 {

  public static void main(String[] args) {
    // Implementiere eine Funktion "likePhoto", diese nimmt zwei Argumente entgegen:
    // 1. Die aktuelle Summe an Likes für dieses Photo "likesCount"
    // 2. Ein Kommentar "comment"
    // Die Funktion sollte zunächst das Kommentar ausgeben, sofern eins angegeben wurde.
    // Anschließend soll die Summe der Likes um 1 erhöht werden und die neue Gesamtsumme auf die Konsole geschrieben werden.
  }
}
