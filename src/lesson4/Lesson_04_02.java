package lesson4;

import static common.Helper.playRoulette;

public class Lesson_04_02 {

  public static void main(String[] args) {
    // Implementiere das Martingale-Spiel: Du besitzt Geldsumme von 40950 Dollar und das Ziel besteht darin, den Betrag auf
    // 81900 Dollar zu verdoppeln (40950 Dollar Gewinn). Der Minimaleinsatz beträgt 10 Dollar. Wann immer du verlierst,
    // verdoppelst du deinen Einsatz, um den bisherigen Verlust auszugleichen und trotzdem einen Gewinn in Höhe des
    // Minimaleinsatzes zu machen.
    // Solltest du gewonnen haben, startest du erneut mit dem Minimaleinsatz von 10 Dollar. Die Funktion playRoulette() sagt dir
    // jedes Mal, ob du gewonnen hast. Der Maximaleinsatz am Tisch beträgt 20480 Dollar: Das bedeutet,
    // du darfst maximal 12 x Spielen ohne zu gewinnen: Das sollte jedoch kein Problem sein - oder?

  }

  public void martingale() {
    int money = 40950;
    int target = 81900;
    int bet = 10;
    boolean win = playRoulette();
  }
}
