# Buchladen 
Wir wollen einen Buchladen implementieren und benötigen dafür zwei Klassen: Book und Bookstore.

Book
* ein Buch hat die Attribute isbn (int), author, title und price (double)
* ein Buch hat die Methode toString(), die folgenden String zurückgibt: "title by author (price)"

Bookstore
* ein Buchladen kann eine Anzahl von Büchern in einem Array speichern
* die maximale Anzahl von Büchern beträgt 5
* folgende Methoden werden gebraucht:
** add(Book book) --- speichert ein Buch im Array und printet bei erfolgreicher Speicherung das Buch mittels System.out.println aus. Nutzt dafür die toString() Methode von Book.
** search(int isbn) --- durchsucht alle vorhandenen Bücher nach der gegebenen ISBN und gibt ggf das gefundene Buch zurück
** int amount() --- gibt die Gesamtanzahl von gespeicherten Büchern zurück

Hinweis: ihr werdet neben dem Bücher-Array noch mindestens ein weiteres Attribut im Bookstore benötigen.

Frage: was passiert, wenn ein 6. Buch eingefügt wird? Warum ist das so?


Main-Methode
* schau dir die vorhandene main-Methode an und aktualisiere sie.

BONUS
- eine weitere Methode in Bookstore gibt das günstigste Buch im Sortiment zurück
- sobald ein 6. Buch eingegeben wird, tritt kein Fehler auf, sondern es wird "Sorry, das Lager ist voll!" per System.out.println ausgegeben.
