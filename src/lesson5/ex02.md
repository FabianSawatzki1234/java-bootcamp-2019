# Radio

Folgende Attribute werden benötigt:
* eingeschaltet: speichert, ob das Radio an oder aus ist
* lautstaerke: die Lautstärke (unterstützte Lautstärke ist zwischen 0 und 10)
* frequenz: die Frequenz des gewählten Senders (erlaubter Frequenzbereich ist zwischen 85.0 und 110.0).

Implementiere zwei Konstruktoren:
* Radio()
* Radio(boolean istAn, int lautstaerke, double frequenz)

Zu der Klasse Radio sollen folgende Methoden implementiert werden:
* an(), aus(): Diese Methoden sollen den Zustand des Attributs eingeschaltet ändern.
* lauter(), leiser(): Diese Methoden sollen die Lautstärke ändern (nur möglich wenn das Radio eingeschaltet ist).
* toString(): Diese Methode soll den internen Zustand in der Form “Radio an: Freq=90.4, Lautstärke=3” zurückgeben.
* waehleSender(double frequenz): Diese Methode soll eine Frequenz speichern. Ist die gewählte Frequenz außerhalb der erlaubten Frequenzbereichs, so soll die Frequenz 99.9 gewählt werden.