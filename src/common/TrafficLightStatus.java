package common;

import java.util.List;
import java.util.Random;

public enum TrafficLightStatus {
  RED("red"),
  YELLOW("yellow"),
  GREEN("green");

  private String status;

  TrafficLightStatus(final String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  public static String randomTrafficStatus() {
    return List.of(values()).get(new Random().nextInt(values().length)).getStatus();
  }
}
