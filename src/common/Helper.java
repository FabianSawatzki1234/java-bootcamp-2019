package common;

import java.util.Random;

public final class Helper {

  public static int getRandomIntBetween(int min, int max) {
    Random rand = new Random();

    return rand.nextInt((max - min) + 1) + min;
  }

  public static boolean playRoulette() {
    Random random = new Random();
      return random.nextFloat() < 0.4864;
  }

}
